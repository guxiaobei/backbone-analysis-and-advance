(function()
{

	var App =
	{
		Models: {},
		Collections: {},
		Views: {},
		Helpers: {}
	};

	App.Helpers.template = function( id )
	{
		return _.template($('#' + id).html())
	};

	App.Models.User = Backbone.Model.extend(
	{
		defaults:
		{
			username: 'abc',
			password: 'def',
			createDate: new Date()
		}
	});

	App.Views.User = Backbone.View.extend(
	{
		tagName: 'li',

		render: function()
		{
			this.$el.html(App.Helpers.template('userTemplate')(this.model.toJSON()));
			return this;
		}
	});

	App.Collections.User = Backbone.Collection.extend(
	{
		model: App.Models.User
	});

	App.Views.Users = Backbone.View.extend(
	{
		tagName: 'ul',

		render: function()
		{
			this.collection.each(function( user )
			{
				var userView = new App.Views.User(
				{
					model: user
				});

				this.$el.append(userView.render().el);
			}, this);

			return this;
		}
	});

	var usersCollection = new App.Collections.User(
	[
	    {
		    username: 'A',
		    password: 'a',
		    createDate: new Date()
	    },
	    {
		    username: 'B',
		    password: 'b',
		    createDate: new Date()
	    },
	    {
		    username: 'C',
		    password: 'c',
		    createDate: new Date()
	    }
	]);

	var usersView = new App.Views.Users(
	{
		collection: usersCollection
	});

	$(document.body).append(usersView.render().el);

})();
