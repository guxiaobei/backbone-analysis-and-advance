var template = function( id )
{
	return _.template($('#' + id).html())
};

var User = Backbone.Model.extend(
{
	defaults:
	{
		username: 'abc',
		password: 'def',
		createDate: new Date()
	}
});

var UserView = Backbone.View.extend(
{
	tagName: 'li',

	render: function()
	{
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},

	template: template('userTemplate')
});

var UsersCollection = Backbone.Collection.extend(
{
	model: User
});

var UsersView = Backbone.View.extend(
{
	tagName: 'ul',

	render: function()
	{
		this.collection.each(function( user )
		{
			var userView = new UserView(
			{
				model: user
			});

			this.$el.append(userView.render().el);
		}, this);

		return this;
	}
});

var usersCollection = new UsersCollection(
[
    {
	    username: 'A',
	    password: 'a',
	    createDate: new Date()
    },
    {
	    username: 'B',
	    password: 'b',
	    createDate: new Date()
    },
    {
	    username: 'C',
	    password: 'c',
	    createDate: new Date()
    }
]);

var usersView = new UsersView(
{
	collection: usersCollection
});

$(document.body).append(usersView.render().el);